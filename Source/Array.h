//
//  Array.h
//  CommandLineTool
//
//  Created by Cameron MacPhail on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Array__
#define __CommandLineTool__Array__

#include <iostream>
class Array
{
public:
    Array();/** Constructor */
    ~Array();/** Destructor */
    void add (float itemValue);
    float get(int index);
    int size();
private:
    float* data;
    int arraySize;
    
};
#endif /* defined(__CommandLineTool__Array__) */
