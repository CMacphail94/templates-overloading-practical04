//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Number.h"

template <typename Type>
Type max (const Type a, const Type b)
{
    if (a < b)
        return b;
    else
        return a;
}

template <typename Type>
Type min (const Type a, const Type b)
{
    if (a < b)
        return a;
    else
        return b;
}

template <typename Type>
Type add (const Type a, const Type b)
{
    return a + b;
}

template <typename Type>
void print (const Type array[], int length)
{
    for(int i = 0;i < length;i++)
    {
        std::cout << array[i];
    }
}

template <typename Type>
double getAverage(const Type array[], int length)
{
    double sum = 0;
    for(int i = 0;i <= length;i++)
    {
        sum = sum + array[i];
    }
    sum = sum / (length-1);
    return sum;
}


int main (int argc, const char* argv[])
{

    
    Number<float> numf;
    Number<int> numi;
    Number<std::string> numstr;
    
    numf.set (0.5f);
    numi.set(2);
    numstr.set("yess");
    
    std::cout << numf.get() << "\n";
    std::cout << numi.get() << "\n";
    std::cout << numstr.get() << "\n";
    

    
   
}

