//
//  Array.cpp
//  CommandLineTool
//
//  Created by Cameron MacPhail on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "Array.h"


Array::Array()
{
    data = nullptr;
    arraySize = 0;
    
}/** Constructor */
Array::~Array()
{
    if (data != nullptr)
    {
        delete[] data;
    }
    
}/** Destructor */
void Array::add (float itemValue)
{
    int i;
    float* tempData = new float[arraySize + 1];
    for(i=0;i < arraySize; i++)
    {
        tempData[i] = data[i];
    }
    
    tempData[arraySize] = itemValue;
    
    if (data != nullptr)
    {
        delete[] data;
    }
    arraySize++;
    data = tempData;
}
float Array::get(int index)
{
    return data[index];
}
int Array::size()
{
    return arraySize;
}
